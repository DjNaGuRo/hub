# Changelog
All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-09-19
* Check existence of the rego policy directory

## [0.1.0] - 2022-09-09
* Initial version